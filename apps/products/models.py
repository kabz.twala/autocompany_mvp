from django.db import models
from django.conf import settings

from apps.utils.models import CreatedModifiedMixin


class Product(CreatedModifiedMixin):
    '''
    Stores autocompany car parts
    '''

    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, blank=True, null=True)

    name = models.CharField(max_length=128, blank=False, null=False)
    decription = models.TextField(blank=True, null=True)
    price = models.DecimalField(default=0.00, max_digits=9, decimal_places=2)

    is_active = models.BooleanField(default=True)  # Should appear in product list?

    '''
    -- nice to have --
    image
    stock
    barcode
    manufacturer
    discount
    reviews/ratings
    '''
    
    def __unicode__(self):
        return f'{self.id}. {self.name}'

    class Meta:
        verbose_name = 'Product'
        verbose_name_plural = 'Products'