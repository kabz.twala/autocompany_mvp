from django.contrib import admin

from apps.products.models import Product


class ProductAdmin(admin.ModelAdmin):

    search_fields = (
        'id',
        'user__username',
        'user__email',
        'name',
        'decription',
        'price',
    )
    ordering_fields = (
        'id',
        '__unicode__',
        'created',
        'modified'
    )
    list_display = (
        '__unicode__',
        'is_active',
        'created',
        'modified'
    )

    class Meta:
        model = Product

admin.site.register(Product, ProductAdmin)
