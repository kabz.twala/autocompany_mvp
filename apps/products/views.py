from rest_framework import viewsets
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.reverse import reverse as api_reverse

from apps.products.models import Product
from apps.products.serializers import ProductSerializer
from apps.utils.permissions import IsStaffOrSuperuser


class ProductViewSet(viewsets.ModelViewSet):

    model = Product
    serializer_class = ProductSerializer
    queryset = Product.objects.filter(is_active=True).order_by('-modified')

    search_fields = (
        'id',
        'user__username',
        'user__email',
        'name',
        'decription',
        'price',
    )
    ordering_fields = (
        'id',
        'created',
        'modified',
    )

    def get_permissions(self):
        # give admin user priviledge to create and update products
        if self.action in ['update', 'partial_update', 'destroy', 'create']:
            self.permission_classes = [IsStaffOrSuperuser]        
        else:
            self.permission_classes = [AllowAny]
        return super().get_permissions()
