from django.db import transaction

from rest_framework import serializers
from rest_framework.reverse import reverse as api_reverse

from apps.products.models import Product


class ProductSerializer(serializers.ModelSerializer):
    
    is_active = serializers.BooleanField(initial=True)

    details_uri = serializers.SerializerMethodField(read_only=True)
    add_to_cart_uri = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Product
        fields = ('id', 'user', 'name', 'decription', 'price', 'is_active', 'add_to_cart_uri', 'details_uri')
        read_only_fields = ('id', 'user')

    def get_details_uri(self, obj):
        request = self.context.get('request')
        return api_reverse('autocompany-products-detail', kwargs={'pk': obj.id}, request=request)

    def get_add_to_cart_uri(self, obj):
        request = self.context.get('request')
        if request.user.is_authenticated:
            return api_reverse('autocompany-carts-add', kwargs={'product_id': obj.id}, request=request)
        return api_reverse('autocompany-login-list', request=request)
                
    def create(self, validated_data):
        user = self.context['request'].user

        if user.is_superuser or user.is_staff:
            with transaction.atomic():
                try:
                    validated_data['user'] = user
                    instance = Product.objects.create(**validated_data)
                except Exception as e:
                    raise serializers.ValidationError(e) 
                return instance

        raise serializers.ValidationError('User is not permitted')

    def update(self, instance, validated_data):
        user = self.context['request'].user
        
        if user.is_superuser or user.is_staff:
            for attr, value in validated_data.items():
                setattr(instance, attr, value)
                
            instance.save()
            return instance

        raise serializers.ValidationError('User is not permitted')
