from django.contrib import admin

from apps.carts.models import Cart, CartItem


class CartItemInline(admin.TabularInline):
    model = CartItem
    fields = [
        'product',
        'quantity',
    ]
    extra = 1


class CartAdmin(admin.ModelAdmin):

    search_fields = (
        'user__username',
        'user__first_name'
    )
    ordering_fields = (
        'id',
        '__unicode__',
        'created',
        'modified'
    )
    list_display = (
        '__unicode__',
        'is_cleared',
        'created',
        'modified'
    )
    inlines = [
        CartItemInline,
    ]

    class Meta:
        model = Cart

admin.site.register(Cart, CartAdmin)
