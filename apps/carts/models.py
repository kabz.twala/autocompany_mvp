from django.db import models
from django.db.models.aggregates import Sum
from django.conf import settings

from apps.products.models import Product
from apps.utils.models import CreatedModifiedMixin


class Cart(CreatedModifiedMixin):
    '''
    Keeps track of items added to the client's cart
    '''

    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, blank=True, null=True)

    is_cleared = models.BooleanField(default=False)

    def __unicode__(self):
        return f'{self.id}. {self.user.username}'
    
    class Meta:
        verbose_name = 'Cart'
        verbose_name_plural = 'Carts'

    @property
    def total(self):
        # tallies the total cost of each product * quantity
        total = self.cartitem.annotate(
            cart_total=models.F('product__price') * models.F('quantity')
        ).aggregate(Sum('cart_total'))['cart_total__sum'] or 0

        return total

    @classmethod
    def get_queryset(cls, user):

        if user.is_superuser or user.is_staff:
            return Cart.objects.select_related('user').all()
        elif user.is_authenticated:
            return Cart.objects.select_related('user').filter(user=user, is_cleared=False)
        return Cart.objects.none()
       

class CartItem(CreatedModifiedMixin):
    '''
    Products added to cart instance
    '''
    
    product = models.ForeignKey(Product, on_delete=models.CASCADE, blank=False, null=False)
    
    cart = models.ForeignKey(Cart, null=False, blank=False, on_delete=models.CASCADE, related_name='cartitem')

    quantity = models.PositiveIntegerField(default=0)
    
    class Meta:
        verbose_name = 'Cart Item'
        verbose_name_plural = 'Cart Items'

    def __unicode__(self):
        return f"{self.id}. {self.product.name}"

    @classmethod
    def get_queryset(cls):
        return CartItem.objects.select_related('product').all()
        
