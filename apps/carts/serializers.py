from django.utils import timezone

from rest_framework import serializers
from rest_framework.reverse import reverse as api_reverse

from apps.carts.models import Cart, CartItem


class CartItemSerializer(serializers.ModelSerializer):

    price = serializers.SerializerMethodField()
    add = serializers.SerializerMethodField()
    remove = serializers.SerializerMethodField()

    class Meta:
        model = CartItem
        fields = ('id', 'product', 'price', 'quantity', 'add', 'remove')
        read_only_fields = ('id', )
    
    def get_price(self, obj):
        return obj.product.price
    
    def get_add(self, obj):
        request = self.context.get('request')
        return api_reverse('autocompany-carts-add', kwargs={'product_id': obj.product.id}, request=request)
    
    def get_remove(self, obj):
        request = self.context.get('request')
        return api_reverse('autocompany-carts-remove', kwargs={'product_id': obj.product.id}, request=request)


class CartSerializer(serializers.ModelSerializer):

    cartitems = serializers.SerializerMethodField()
    
    details_uri = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Cart
        fields = ('id', 'user', 'cartitems', 'is_cleared', 'total', 'details_uri')
        read_only_fields = ('id', 'user', 'is_cleared', 'total')
    
    def get_cartitems(self, obj):
        return [CartItemSerializer(p, context=self.context).data for p in obj.cartitem.select_related('product').all()]

    def get_details_uri(self, obj):
        request = self.context.get('request')
        return api_reverse('autocompany-carts-detail', kwargs={'pk': obj.id}, request=request)
    
    def create(self, validated_data):
        raise serializers.ValidationError('Not permitted')

    def update(self, validated_data):
        raise serializers.ValidationError('Not permitted')


class CheckoutSerializer(serializers.ModelSerializer):

    first_name = serializers.CharField(source='user.first_name', required=True)
    last_name = serializers.CharField(source='user.last_name', required=True)

    delivery_date = serializers.DateTimeField(default=timezone.now(), format="%d-%m-%Y %H:%M")

    order_type_choices =( 
        ("0", "Pick up - Leave out address"), 
        ("1", "Delivery - Requires address"),
    )
    order_type = serializers.ChoiceField(default=0, choices=order_type_choices)

    address = serializers.CharField(required=False)

    cartitems = serializers.SerializerMethodField()

    class Meta:
        model = Cart
        fields = ('id', 'total', 'first_name', 'last_name', 'delivery_date', 'order_type', 'address', 'cartitems')
        read_only_fields = ('id', 'cartitems')

    def get_cartitems(self, obj):
        try:
            return [CartItemSerializer(p, context=self.context).data for p in obj.cartitem.select_related('product').all()]
        except:
            return []

    def get_total(self, obj):
        return obj.cart.total or 0

    def create(self, validated_data):
        raise serializers.ValidationError('Not permitted')

    def update(self, validated_data):
        raise serializers.ValidationError('Not permitted')