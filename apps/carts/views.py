from datetime import datetime

from django.shortcuts import redirect
from django.db import transaction
from django.utils import timezone

from rest_framework import viewsets, decorators, status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.reverse import reverse as api_reverse

from apps.carts.models import Cart, CartItem
from apps.carts.serializers import CartSerializer, CheckoutSerializer
from apps.orders.models import Order
from apps.products.models import Product
from apps.utils.permissions import GrantNoPermission


class CartViewSet(viewsets.ModelViewSet):

    model = Cart
    serializer_class = CartSerializer

    search_fields = (
        'id',
        'user__username',
        'user__first_name'
    )
    ordering_fields = (
        'id',
        'created',
        'modified',
    )

    def get_queryset(self):
        return Cart.get_queryset(self.request.user).order_by('-modified')

    def get_permissions(self):
        # give authenticated clients access to view cart list
        self.permission_classes = [IsAuthenticated]
        # remove client/admin ability to add order manually or update cart.
        if self.action in ['create', 'update', 'partial_update', 'destroy']:
            self.permission_classes = [GrantNoPermission]
        return super().get_permissions()

    def get_paginated_response(self, data):
        response = super().get_paginated_response(data)
        if len(response.data['results']) > 0:
            # add clear uri links if there are items in the cart
            response.data['clear'] = api_reverse('autocompany-carts-clear', request=self.request)
        response.data['orders'] = api_reverse('autocompany-orders-list', request=self.request)
        return Response(data=response.data)

    @decorators.action(detail=False, methods=['get', 'put'], url_path='clear')
    def clear(self, request, *args, **kwargs):
        '''
        Clear contents of cart
        '''
        Cart.objects.filter(user=request.user, is_cleared=False).update(is_cleared=True)
        return Response({'detail': 'Cart is now empty'}, status=status.HTTP_200_OK) 
    
    @decorators.action(detail=True, methods=['get'], url_path='order-details')
    def order_details(self, request, pk=None):
        '''
        Redirect user to view specific order or their list of orders
        '''
        try:
            order = Order.objects.get(cart__id=pk, cart__user=request.user, cart__is_cleared=True)
            return redirect(api_reverse('autocompany-orders-detail', kwargs={'pk': order.id}, request=request))
        except Exception as e:
            return redirect(api_reverse('autocompany-orders-list', request=request))

    @decorators.action(detail=False, methods=['get', 'post'], url_path='add/(?P<product_id>\d+)')
    def add(self, request, product_id=None):
        '''
        Add one product to cart
        '''
        try:
            product = Product.objects.get(id=product_id)
            
            cart, _ = Cart.objects.get_or_create(user=request.user, is_cleared=False)

            cart_item, _ = CartItem.objects.get_or_create(cart=cart, product=product)
            cart_item.quantity += 1
            cart_item.save()

            return Response({'detail': f'{product.name} added. Quantity: {cart_item.quantity}'}, status=status.HTTP_200_OK)
        except Exception as e:
            return Response({'detail': str(e)}, status=status.HTTP_400_BAD_REQUEST)

    @decorators.action(detail=False, methods=['get', 'delete'], url_path='remove/(?P<product_id>\d+)')
    def remove(self, request, product_id=None):
        '''
        Reduce product quantity or remove one product from cart
        '''
        try:
            product = Product.objects.get(id=product_id)
            
            try:
                cart = Cart.objects.get(user=request.user, is_cleared=False)

                cart_item = CartItem.objects.get(cart=cart, product=product)
                if cart_item.quantity > 0:
                    cart_item.quantity -= 1
                    cart_item.save()
                else:
                    cart_item.delete()

                return Response({'detail': f'{product.name} removed. Quantity: {cart_item.quantity|0}'}, status=status.HTTP_200_OK)
            except Cart.DoesNotExist:
                return Response({'detail': 'Nothing to remove'}, status=status.HTTP_204_NO_CONTENT) 
        except Exception as e:
            return Response({'detail': str(e)}, status=status.HTTP_400_BAD_REQUEST)


class CheckoutViewSet(viewsets.GenericViewSet):
    '''
    Checkout
    '''

    model = Cart
    permission_classes = [IsAuthenticated]
    serializer_class = CheckoutSerializer
    
    def list(self, request, *args, **kwargs):
        # redirect to cart page if there is nothing in cart
        try:
            instance = Cart.objects.select_related('user').get(user=request.user, is_cleared=False)
            instance.cartitem.all()[0]
            serializer = self.get_serializer(instance)
            return Response(serializer.data)
        except Exception as e:
            return redirect(api_reverse('autocompany-carts-list', request=request))
    
    def create(self, request, *args, **kwargs):
        try:
            instance = Cart.objects.select_related('user').get(user=request.user, is_cleared=False)

            serializer = self.get_serializer(data=request.data)
            serializer.is_valid(raise_exception=True)
            
            # roll back if there are any errors
            with transaction.atomic():

                delivery_date = datetime.strptime(serializer.data['delivery_date'], '%d-%m-%Y %H:%M')
                delivery_time = timezone.make_aware(delivery_date, timezone.get_default_timezone())

                # check if date has not passed as yet - weak check against current time
                if delivery_time < timezone.now():
                    return Response({'detail': 'Delivery date has passed already.'}, status=status.HTTP_400_BAD_REQUEST)
                
                address = ''
                if 'address' in serializer.data:
                    address = f'Address: {serializer.data["address"]}'

                # check if delivery was selected, if yes - make sure the address was given
                if int(serializer.data['order_type']) == 1:
                    if address == '':
                        return Response({'detail': 'Delivery address is required'}, status=status.HTTP_400_BAD_REQUEST)

                # add cart items to order instance and mark cart as clear
                instance.is_cleared = True
                instance.save()

                Order.objects.create(cart=instance, delivery_date=delivery_time,
                    order_type=int(serializer.data['order_type']), 
                    details=f'First name: {serializer.data["first_name"]}, Last Name: {serializer.data["last_name"]}, {address}'
                )
            
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        except Exception as e:
            return Response({'detail': str(e)}, status=status.HTTP_400_BAD_REQUEST)
        