from rest_framework import pagination
from rest_framework.response import Response
from rest_framework.reverse import reverse as api_reverse


class CustomPagination(pagination.PageNumberPagination):
    page_size_query_param = 'page_size'
    max_page_size = 100
    page_size = 20

    def get_paginated_response(self, data):

        default_url = api_reverse('autocompany-products-list', request=self.request)
        if not self.request.user.is_authenticated:
            default_url = api_reverse('autocompany-login-list', request=self.request)

        return Response({
            'paginator': {'links': {
                'next': self.get_next_link(),
                'previous': self.get_previous_link()
            },
                'count': self.page.paginator.count,
                'page_number': self.page.number,
                'num_pages': self.page.paginator.num_pages,
            },
            'is_authenticated':self.request.user.is_authenticated,
            'default_url': default_url,
            'cart': api_reverse('autocompany-carts-list', request=self.request),
            'checkout': api_reverse('autocompany-checkout-list', request=self.request),
            'results': data
        })