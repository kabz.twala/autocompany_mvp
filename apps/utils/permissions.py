from rest_framework.permissions import BasePermission


class IsStaffOrSuperuser(BasePermission):
    """
    Custom permission to only allow superiser or staff to apply changes.
    """
    message = "You don't have permission to access"

    def has_permission(self, request, view):
        return request.user.is_superuser or request.user.is_staff
            
    def has_object_permission(self, request, view, obj):
        return request.user.is_superuser or request.user.is_staff


class GrantNoPermission(BasePermission):
    """
    Object-level permission deny all users including admin | use cautiously
    """
    message = "You don't have permission to access"

    def has_permission(self, request, view):
        return False
        
    def has_object_permission(self, request, view, obj):
        return False