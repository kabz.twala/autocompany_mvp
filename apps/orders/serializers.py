from rest_framework import serializers
from rest_framework.reverse import reverse as api_reverse

from apps.carts.serializers import CartSerializer
from apps.orders.models import Order


class OrderSerializer(serializers.ModelSerializer):
    
    cart = serializers.SerializerMethodField()
    order_type_text = serializers.SerializerMethodField(read_only=True)
    status_text = serializers.SerializerMethodField(read_only=True)

    details_uri = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Order
        fields = ('id', 'delivery_date', 'order_type', 'order_type_text', 'status', 'status_text', 'details', 'details_uri', 'cart')
        read_only_fields = ('id', 'cart', 'delivery_date', 'order_type')
    
    def get_cart(self, obj):
        return [CartSerializer(obj.cart, context=self.context).data]
    
    def get_order_type_text(self, obj):
        return Order._ORDER_TYPE[obj.order_type]
    
    def get_status_text(self, obj):
        return Order._STATUS_TYPE[obj.status]
    
    def get_cart(self, obj):
        return [CartSerializer(obj.cart, context=self.context).data]
               
    def get_details_uri(self, obj):
        request = self.context.get('request')
        return api_reverse('autocompany-orders-detail', kwargs={'pk': obj.id}, request=request)
     
    def create(self, validated_data):
        raise serializers.ValidationError('User is not permitted')

    def update(self, instance, validated_data):

        user = self.context['request'].user
        
        if user.is_superuser or user.is_staff:
            for attr, value in validated_data.items():
                setattr(instance, attr, value)
                
            instance.save()
            return instance

        raise serializers.ValidationError('User is not permitted')
