from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated, IsAdminUser

from apps.orders.models import Order
from apps.orders.serializers import OrderSerializer
from apps.utils.permissions import GrantNoPermission


class OrderViewSet(viewsets.ModelViewSet):

    model = Order
    serializer_class = OrderSerializer

    search_fields = (
        'id',
        'cart__user__username',
        'cart__user__email',
    )
    ordering_fields = (
        'id',
        'cart__delivery_date',
        'created',
        'modified',
    )

    def get_permissions(self):
        # give authenticated clients access to view list and any specific orders
        self.permission_classes = [IsAuthenticated]
        # remove client ability to add order manually or update.
        if not self.action in ['list', 'retrieve']:
            self.permission_classes = [GrantNoPermission]
            # only the autocompany can update orders e.g. change order status 
            if self.action in ['update']:
                self.permission_classes = [IsAdminUser]

        return super().get_permissions()

    def get_queryset(self):
        return Order.get_queryset(self.request.user).order_by('-modified')
