from django.db import models

from apps.carts.models import Cart
from apps.utils.models import CreatedModifiedMixin


class Order(CreatedModifiedMixin):
    '''
    Stores succussful checkout data for autocompany to handle

    - cart: Cart object 
    - order_type: client determines whether they 
        will pick up the goods or they want the goods delivered
    - status: when an order is made, the status is set to Processing by default.
        The autocompany can update the status when goods are delivered/collected
    '''

    cart = models.ForeignKey(Cart, on_delete=models.CASCADE)

    delivery_date = models.DateTimeField()
    
    _ORDER_TYPE = {
        0: 'Pick up',
        1: 'Delivery',
    }
    order_type_choices = (
        (0, 'Pick up'),
        (1, 'Delivery'),
    )
    order_type = models.PositiveIntegerField(default=0, choices=order_type_choices)

    _STATUS_TYPE = {
        0: 'Processing',
        1: 'Delivered',
        2: 'Cancelled'
    }
    status_choices = (
        (0, 'Processing'),
        (1, 'Delivered'),
        (2, 'Cancelled'),
    )
    status = models.PositiveIntegerField(default=0, choices=status_choices)

    details = models.TextField(blank=True, null=True)

    '''
    -- nice to have --
    reference
    '''

    def __unicode__(self):
        return f'{self.id}. {self.cart}'

    class Meta:
        verbose_name = 'Order'
        verbose_name_plural = 'Orders'

    @classmethod
    def get_queryset(cls, user):

        if user.is_superuser or user.is_staff:
            return Order.objects.select_related('cart', 'cart__user').all()
        else:
            return Order.objects.select_related('cart', 'cart__user').filter(cart__user=user)
    