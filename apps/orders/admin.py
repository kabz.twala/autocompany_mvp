from django.contrib import admin

from apps.orders.models import Order


class OrderAdmin(admin.ModelAdmin):

    search_fields = (
        'id',
        'cart__user__username',
        'cart__user__email',
    )
    ordering_fields = (
        'id',
        '__unicode__',
        'delivery_date',
        'created',
        'modified'
    )
    list_display = (
        '__unicode__',
        'order_type',
        'delivery_date',
        'created',
        'modified'
    )

    class Meta:
        model = Order

admin.site.register(Order, OrderAdmin)
