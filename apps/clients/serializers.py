from django.contrib.auth import login, authenticate, get_user_model
from django.contrib.auth.password_validation import validate_password
from django.contrib.auth.validators import ASCIIUsernameValidator
from django.core.validators import validate_email
from django.db import transaction

from rest_framework import serializers
from rest_framework.validators import UniqueValidator


class LoginSerializer(serializers.Serializer):

    username = serializers.CharField(
        style={'input_type': 'text', 'placeholder': 'Username', 'autofocus': True},
        required=True, 
    )
    password = serializers.CharField(
        style={'input_type': 'password', 'placeholder': 'Password'},
        write_only=True, required=True
    )

    class Meta:
        model = get_user_model()
        fields = ('id', 'email', 'password')
        read_only_fields = ('id', )
    
    def create(self, validated_data):
        try:
            user = authenticate(self.context.get('request'), username=validated_data['username'], password=validated_data['password'])
            if user is None:
                raise serializers.ValidationError('Incorrect credentials')
            login(self.context.get('request'), user)
            return user
        except Exception as e:
            raise serializers.ValidationError(e)

    def update(self, validated_data):
        raise serializers.ValidationError('User is not permitted')


class RegisterSerializer(serializers.Serializer):

    email = serializers.CharField(
        style={'input_type': 'email', 'placeholder': 'Email', 'autofocus': True},
        required=True, validators=[UniqueValidator(queryset=get_user_model().objects.all()), validate_email]
    )
    username = serializers.CharField(
        style={'input_type': 'text', 'placeholder': 'Username'},
        validators=[ASCIIUsernameValidator()]
    )
    password = serializers.CharField(
        style={'input_type': 'password', 'placeholder': 'Password'},
        write_only=True, required=True, validators=[validate_password]
    )
    password2 = serializers.CharField(
        style={'input_type': 'password', 'placeholder': 'Confirm Password'},
        write_only=True, required=True
    )

    class Meta:
        model = get_user_model()
        fields = ('id', 'email', 'username', 'password', 'password2')
        read_only_fields = ('id', )

    def validate(self, attrs):
        if attrs['password'] != attrs['password2']:
            raise serializers.ValidationError({"password": "Password fields didn't match."})
        return attrs
    
    def create(self, validated_data):
        with transaction.atomic():
            try:
                validated_data.pop('password2')
                user = get_user_model().objects.create_user(**validated_data)
                login(self.context.get('request'), user)
                return user
            except Exception as e:
                raise serializers.ValidationError(e)
        
    def update(self, validated_data):
        raise serializers.ValidationError('User is not permitted')
        
