from django.db import models

from apps.utils.models import CreatedModifiedMixin


class Client(CreatedModifiedMixin):
    '''
    save client related data
    '''
    pass