from django.contrib.auth import get_user_model
from django.shortcuts import redirect

from rest_framework import viewsets
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.reverse import reverse as api_reverse

from apps.clients.serializers import LoginSerializer, RegisterSerializer


class LoginViewSet(viewsets.ModelViewSet):
    '''
    Login as a client
    '''

    model = get_user_model()
    serializer_class = LoginSerializer
    permission_classes = [AllowAny]

    def dispatch(self, request, *args, **kwargs):
        # redirect to client account if logged in
        if request.user.is_authenticated:
            return redirect(api_reverse('autocompany-products-list', request=request))
        return super().dispatch(request, *args, **kwargs)

    def get_paginated_response(self, data):
        # overwrite custom pagination response - remove links and data 
        return Response({
            'is_authenticated':self.request.user.is_authenticated, 
            'default_url': api_reverse('autocompany-register-list', request=self.request)
        })
    
    def get_queryset(self):
        if self.request.user.is_superuser or self.request.user.is_staff:
            return get_user_model().objects.all()
        elif self.request.user.is_authenticated:
            return get_user_model().objects.filter(id=self.request.user.id)
        return get_user_model().objects.none()


class RegisterViewSet(viewsets.ModelViewSet):
    '''
    Register an account as a client
    '''

    model = get_user_model()
    serializer_class = RegisterSerializer
    permission_classes = [AllowAny]

    def dispatch(self, request, *args, **kwargs):
        # redirect to client account if logged in
        if request.user.is_authenticated:
            return redirect(api_reverse('autocompany-products-list', request=request))
        return super().dispatch(request, *args, **kwargs)

    def get_paginated_response(self, data):
        # overwrite custom pagination response - remove links and data 
        return Response({
            'is_authenticated':self.request.user.is_authenticated, 
            'default_url': api_reverse('autocompany-login-list', request=self.request)
        })
    
    def get_queryset(self):
        if self.request.user.is_superuser or self.request.user.is_staff:
            return get_user_model().objects.all()
        elif self.request.user.is_authenticated:
            return get_user_model().objects.filter(id=self.request.user.id)
        return get_user_model().objects.none()