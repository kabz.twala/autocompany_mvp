asgiref==3.5.0
Django==4.0.3
sqlparse==0.4.2
djangorestframework==3.13.1
python-decouple==3.6
psycopg2