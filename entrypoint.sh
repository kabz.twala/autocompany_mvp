#!/bin/sh

SUPERUSER_EMAIL=${DJANGO_SUPERUSER_EMAIL:-"admin@autocompany.com"}
PORT=${APP_PORT:-80}
WORKERS=${APP_WORKERS:-2}

cd /app

python manage.py collectstatic --no-input

python manage.py migrate --no-input

python manage.py createsuperuser --email $SUPERUSER_EMAIL --noinput || true

uwsgi --http :${PORT} --processes ${WORKERS} --static-map /static=/app/static --module autocompany.wsgi:application
