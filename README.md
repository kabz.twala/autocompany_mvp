# Autocompany
An e-commerce mvp store for a company selling specialised car parts

Setup
---------
Let's get started.

    git clone https://gitlab.com/kabz.twala/autocompany_mvp.git

**Environment variable**

Before we go ahead, lets create our environment file

> This file is essential for our project to work

Open your favorite terminal, navigate to the root level of the project (containing manage.py) and run

    cp .env.template .env

We've just created our environment file called `.env` using our template env file.

Next, open the `.env` file using your code editor of choice, and replace the dummy entries with your input. You don't have to use `"` or `'` around your input. e.g. Do not use `DJANGO_SECRET_KEY='strong-key'` but do this instead, `DJANGO_SECRET_KEY=strong-key`. More examples,

    DEBUG=True
    TIME_ZONE=Africa/Johannesburg

> Note: You can leave the values for `POSTGRES_HOST` and `POSTGRES_PORT` the same. If you change these values, you will have to open `docker-compose.yaml` (also found in the root folder) and make the value for `POSTGRES_HOST` the same as the database service name and make the value of `POSTGRES_PORT` the same the database ports. For example

.env file

    ...
    POSTGRES_HOST=some_db_host_name
    POSTGRES_PORT=5499
    ...

docker-compose.yaml file

    ...
    auto_company_app
        container_name: auto_company_app
        build: .
        ...

    some_db_host_name:
        container_name: some_db_host_name
        image: postgres
        command: -p 5499
        ports:
        - "5499:5499"
    ...


If you are comforatble setting up your own values, feel free to do so. Save your changes and lets continue.


**Running the app using Docker**

To run the project, we'll be using docker compose. In the terminal, change directories to the root of the project (containing docker-compose.yaml) and run:

    docker compose up --build -d

This should create your postgresql database, postgresql user and django super administrator. 

> Note: to monitor any errors that might occur, I would recommend us not to use the detach flag `-d` as yet. You might have to run the command above more than once (e.g if it timeouts). 

When everything is running as expected, open your browser and test.

    http://localhost/

You can login as admin to test the db connection using the credentials you have stored in your .env file

    http://localhost/admin/

If you ran docker without the detach flag, cancel the running process and use the detach flag

    crtl c (might have to do this a couple of times)
    docker compose up -d

To stop the services, run

    docker compose down

> Note: If you make changes to the models, you will have to run makemigrations locally before they can reflect on docker. Quick way to do this is to create a new virtual env, run `python manage.py -r requirements.txt` then `python manage.py makemigrations` 


Apps
---------
Throughout our breakdown, we'll be using localhost as our domain for the api endpoints running on port 80. We assume the superuser and staff user belong to the autocompany.

Navigate to 

    http://localhost/api/

We should see the following endpoints which we'll go into depth below

    {
        "products": "http://localhost/api/products/",
        "cart": "http://localhost/api/cart/",
        "checkout": "http://localhost/api/checkout/",
        "orders": "http://localhost/api/orders/",
        "login": "http://localhost/api/login/",
        "register": "http://localhost/api/register/"
    }

> List response cointains

- paginator - manages the list of items that are retrieved from making a query. includes number of items and links to view the next set of items or previous set.
- is_authenticated: boolean value denoting wher the use is logged in or not
- default_url - when logged out, the default url is set to the login endpoint. when authenticated, the default url is set to the products endpoint
- cart: redirects authenticated user to cart page
- checkout: redirects authenticated user to checkout page if there are items in the cart
- results: list of items that have been queried

**Products app**

The products app collects the autocompany car parts data. Key functionality includes:

* anonymous and authenticated clients have an overview and details of all the products.
* only the admin user can add or edit products.
* to add products to the cart, clients must be authenticated.

> List of products

    http://localhost/api/products/

> View product
    
    http://localhost/api/products/{id}/

**Clients app**

For now, this app enables clients to login and register ccounts. 

> Login

    http://localhost/api/login/

> Regigister or signup

    http://localhost/api/register/

**Carts**

The carts app keeps track of items added to the client's cart. Key functionality includes:

* authenticated clients have access to view their cart.
* clients can add, remove and clear items from thir cart.
* clients can order items by checking out.
* clients can set a delivery date. 
* clients can decide to collect the items or decide to get the items delivered to them.

> List of carts

    http://localhost/api/cart/

> View cart

    http://localhost/api/cart/{id}/

> Add product to cart

    http://localhost/api/cart/add/{product_id}/

> Remove product from cart

    http://localhost/api/cart/remove/{product_id}/

> Checkout

    http://localhost/api/checkout/

> Clear cart

    http://localhost/api/clear/

**Orders**

The order app stores cart data after a client has succussfully checked out. Key functionality includes:

* authenticated clients have access to view their orders but cannot edit.
* on creation, the order status is set to `Processing` by default.
* only the company can update the order status when goods are delivered or collected.

> List of orders

    http://localhost/api/orders/

> View order

    http://localhost/api/order/{id}/


How to use
---------

A quick rundown on how to go about using the app.

1. Login as admin

    http://localhost/api/login/

2. Add products - scroll down to add (as many you want)

    http://localhost/api/products/

3. Add products to cart - Either logout and create a client account or stay logged in as admin

    http://localhost/api/products/

> Notice `add_to_cart_uri` on each product. Click on a few products, it's on me ;)

4. View cart - if you are still on the products list page, scroll down and you should see

    http://localhost/api/cart/

5. Checkout - scroll down to fill in form

    http://localhost/api/checkout/

> Note: if there is nothing in cart, checkout redirects to cart list page.

6. View orders

    http://localhost/api/orders/

7. You can add some products and on the cart list page, you can test the clear all function

    http://localhost/api/clear/

> or the remove function

    http://localhost/api/cart/remove/{id}/


Given more time…
---------

If I had more time to work on the project, I would have looked at handling the following scenarios:

- refunds - on cancellation
- sending communication of events via email
- Re-order
- Give clients ability to track orders
- handle errors in models with correct status code responses.
- possibly move actions in carts views.py into models.py as cartitem properties - clean up bulk looking code
- run through test cases

... just to name a few


Weird behaviour
---------

> Note: ORM id numbers might have gaps from one to the next because of roll backs caused by `transactions.atomic()` (e.g. id=3 next id=11) - https://www.postgresql.org/docs/9.5/datatype-numeric.html#DATATYPE-SERIAL