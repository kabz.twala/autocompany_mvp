"""autocompany URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include

from rest_framework import routers

from apps.carts import views as carts_views
from apps.clients import views as clients_views
from apps.orders import views as orders_views
from apps.products import views as products_views

api_autocompany_router = routers.DefaultRouter()

api_autocompany_router.register(r'products', products_views.ProductViewSet, basename='autocompany-products')
api_autocompany_router.register(r'cart', carts_views.CartViewSet, basename='autocompany-carts')
api_autocompany_router.register(r'checkout', carts_views.CheckoutViewSet, basename='autocompany-checkout')
api_autocompany_router.register(r'orders', orders_views.OrderViewSet, basename='autocompany-orders')
api_autocompany_router.register(r'login', clients_views.LoginViewSet, basename='autocompany-login')
api_autocompany_router.register(r'register', clients_views.RegisterViewSet, basename='autocompany-register')

urlpatterns = [
    path('admin/', admin.site.urls),

    path('api/', include(api_autocompany_router.urls)),
    path('api-auth/', include('rest_framework.urls')),
]
